package io.nutz.eureka.server.shared;

import org.nutz.boot.NbApp;
import org.nutz.ioc.loader.annotation.IocBean;

@IocBean
public class EurekaServerLauncher {

    // 端口是8602
    // 首页 http://eureka.nutz.cn/eureka/status
    public static void main(String[] args) throws Exception {
        new NbApp().setPrintProcDoc(true).run();
    }

}
